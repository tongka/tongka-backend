import { EventState } from "../entities/Event";
import { getEventById } from "./get.model";

export const deliverPhoto = async (eventId: number, photoLink: string) => {
    let event = await getEventById(eventId);
    if (!event || event.state !== EventState.DEPOSIT_ACCEPTED) { return false; }
    event.state = EventState.PHOTOS_READY;
    event.photosLink = photoLink;
    event.deliverDate = new Date();
    event = await event.save();
    return event;
};
