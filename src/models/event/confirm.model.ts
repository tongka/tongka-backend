import { EventState } from "../entities/Event";
import { getEventById } from "./get.model";

export const acceptEvent = async (eventId: number) => {
    let event = await getEventById(eventId);
    if (!event || event.state !== EventState.REQUESTED) { return false; }
    event.state = EventState.ACCEPTED;
    event = await event.save();
    return true;
};

export const rejectEvent = async (eventId: number) => {
    const event = await getEventById(eventId);
    if (!event || event.state !== EventState.REQUESTED) { return false; }
    await event.remove();
    return true;
};
