import { EventState } from "../entities/Event";
import { User } from "../entities/User";
import { getEventById } from "./get.model";

export const cancelEvent = async (eventId: number, user: User) => {
    let event = await getEventById(eventId);
    if (!event || (EventState.ACCEPTED >= event.state && event.state >= EventState.DEPOSIT_ACCEPTED)) { return false; }
    event.isCancel = 1;
    if (event.state === EventState.ACCEPTED || user.type === "customer") {
        event.state = EventState.NO_REDEEM_CANCELLED;
        event.isClose = 1;
    } else {
        event.state = EventState.REDEEM_CANCELLED;
    }
    event = await event.save();
    return true;
};
