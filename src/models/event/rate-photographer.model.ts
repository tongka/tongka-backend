import { EventState } from "../entities/Event";
import { Rating } from "../entities/Rating";
import { getEventById } from "./get.model";

export const ratePhotographer = async (eventId: number, score: number) => {
    const event = await getEventById(eventId);
    if (!event || (event.state !== EventState.PHOTOS_READY && event.state !== EventState.TOTAL_PAID)) {
        return false;
    }
    let rating = await Rating.findOne({
        where: {
            service: event.service.id,
            user: event.customer.id
        }
    });
    let photographer = event.service.photographer;
    if (!photographer) { return false; }
    if (!photographer.rating) { photographer.rating = 0; }
    if (rating) {
        photographer.rating = (photographer.rating * photographer.reviewCount - rating.score + score)
            / photographer.reviewCount;
    } else {
        photographer.rating = (photographer.rating * photographer.reviewCount++ + score) / photographer.reviewCount;
    }
    photographer = await photographer.save();
    rating = new Rating(event.customer, event.service, score);
    rating = await rating.save();
    return rating;
};
