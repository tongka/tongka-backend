import { EventState } from "../entities/Event";
import { getEventById } from "./get.model";

export const acceptPayment = async (eventId: number) => {
    let event = await getEventById(eventId);
    if (!event || (event.state !== EventState.DEPOSIT_PAID && event.state !== EventState.TOTAL_PAID)) { return false; }
    if (event.state ===  EventState.DEPOSIT_PAID) {
        event.state = EventState.DEPOSIT_ACCEPTED;
        event.paidAmount = event.price * 0.3;
    } else if (event.state === EventState.TOTAL_PAID) {
        event.state = EventState.COMPLETED;
        event.paidAmount = event.price;
        event.isClose = 1;
        event.service.eventCount++;
        if (event.service.photographer) { event.service.photographer.eventCount++; }
    }
    event = await event.save();
    return true;
};

export const acceptRedeem = async (eventId: number) => {
    let event = await getEventById(eventId);
    if (!event || (event.state !== EventState.DEPOSIT_RETURNED)) { return false; }
    event.state = EventState.DEPOSIT_RETURN_ACCEPTED;
    event.paidAmount = 0;
    event.isClose = 1;
    event = await event.save();
    return true;
};
