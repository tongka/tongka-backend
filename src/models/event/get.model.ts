import _, { Dictionary } from "lodash";
import { getRepository } from "typeorm";
import { Event } from "../entities/Event";
import { getProfile } from "../user/get-profile.model";

export const getPhotographerEvents = async (userId: number): Promise<Event[]> => {
    const events: Event[] = await getRepository(Event)
        .createQueryBuilder("event")
        .innerJoinAndSelect("event.service", "service")
        .innerJoinAndSelect("service.photographer", "photographer")
        .leftJoinAndSelect("service.coverPhoto", "coverPhoto")
        .innerJoinAndSelect("event.customer", "customer")
        .leftJoinAndSelect("event.depositSlip", "depositSlip")
        .leftJoinAndSelect("event.totalSlip", "totalSlip")
        .leftJoinAndSelect("event.redeemSlip", "redeemSlip")
        .where("service.photographer.id = :userId", { userId })
        .getMany();

    return events;
};

export const getCustomerEvents = async (userId: number): Promise<Event[]> => {
    const events: Event[] = await getRepository(Event)
        .createQueryBuilder("event")
        .innerJoinAndSelect("event.service", "service")
        .innerJoinAndSelect("service.photographer", "photographer")
        .leftJoinAndSelect("service.coverPhoto", "coverPhoto")
        .innerJoinAndSelect("event.customer", "customer")
        .leftJoinAndSelect("event.depositSlip", "depositSlip")
        .leftJoinAndSelect("event.totalSlip", "totalSlip")
        .leftJoinAndSelect("event.redeemSlip", "redeemSlip")
        .where("event.customer.id = :userId", { userId })
        .getMany();

    return events;
};

export const getEvents = async (userId: number): Promise<Event[]> => {
    const user = await getProfile(userId);
    if (!user) {
        throw new Error("User not found");
    }
    if (user.type === "photographer") {
        return await getPhotographerEvents(userId);
    } else {
        return await getCustomerEvents(userId);
    }
};

export const getEventById = async (id: number): Promise<Event | undefined> => {
    return await getRepository(Event).findOne(id, {
        relations: [
            "service", "service.photographer", "service.coverPhoto", "customer", "depositSlip",
            "totalSlip", "redeemSlip"
        ]
    });
};

export const getUnfinishedEvents = async (userId: number): Promise<Event[]> => {
    let events = await getEvents(userId);
    events = events.filter(event => event.isClose === 0);
    return events;
};

export const getGroupedEvents = async (userId: number): Promise<Dictionary<Event[]>> => {
    const events = await getEvents(userId);

    return _.groupBy(events, event => event.isClose ? 11 : event.state);
};
