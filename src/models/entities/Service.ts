import {
    BaseEntity, Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne, PrimaryGeneratedColumn
} from "typeorm";
import { Photo } from "./Photo";
import { Tag } from "./Tag";
import { User } from "./User";

@Entity()
export class Service extends BaseEntity {
    @PrimaryGeneratedColumn("increment")
    public readonly id?: number;

    @Column("varchar", { length: 100, nullable: false })
    public title: string;

    @Column("date", { name: "open_date", nullable: true })
    public openDate?: Date;

    @Column("date", { name: "close_date", nullable: true })
    public closeDate?: Date;

    @Column("double", { name: "half_day_price", nullable: true })
    public halfDayPrice?: number;

    @Column("double", { name: "full_day_price", nullable: true })
    public fullDayPrice?: number;

    @Column("text", { nullable: true })
    public description?: string;

    @Column("tinyint", { nullable: false, default: 1, width: 1 })
    public available: 0 | 1;

    @Column("text", { nullable: true })
    public location?: string;

    @Column("int", { name: "derived_prev_event_count", default: 0 })
    public eventCount: number;

    // @Column("double", { name: "derived_reviews_count", default: 0 })
    // public ratingCount: number;

    // @Column("double", { name: "rating", nullable: true })
    // public rating?: number;

    @ManyToOne(type => User, { onDelete: "CASCADE" })
    @JoinColumn({ name: "photographer_id" })
    public photographer?: User;

    @ManyToMany(type => Tag)
    @JoinTable({
        name: "service_tag",
        joinColumns: [{ name: "service_id" }],
        inverseJoinColumns: [{ name: "tag_id" }]
    })
    public tags?: Tag[];

    @OneToOne(type => Photo)
    @JoinColumn({name: "cover_photo_id"})
    public coverPhoto?: Photo;

    constructor(photographer: User, title: string) {
        super();
        this.photographer = photographer;
        this.title = title;
        this.available = 1;
        this.eventCount = 0;
        // this.ratingCount = 0;
    }
}
