import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from "typeorm";
import { User } from "./User";

@Entity()
export class Photo extends BaseEntity {
    @PrimaryColumn("varchar", { length: 20 })
    public readonly id: string;

    @Column("varchar", { length: 100, unique: true })
    public filepath: string;

    @Column("datetime", { name: "uploaded_time" })
    public uploadedTime: Date;

    @ManyToOne(type => User, user => user.id, { nullable: false })
    @JoinColumn({ name: "owner_id" })
    public owner?: User;

    @Column("tinyint", {name: "show_portfolio"})
    public showPortfolio: 0 | 1;

    constructor(id: string, filepath: string, owner: User, showPortfolio: 0 | 1) {
        super();
        this.id = id;
        this.filepath = filepath;
        this.uploadedTime = new Date();
        this.owner = owner;
        this.showPortfolio = showPortfolio;
    }
}
