import { BaseEntity, Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Service } from "./Service";

@Entity()
export class Tag extends BaseEntity {
    @PrimaryGeneratedColumn("increment")
    public readonly id?: number;

    @Column("varchar", {
        name: "tag_name", length: 100,
        unique: true, nullable: false
    })
    public tagName: string;

    @ManyToMany(type => Service)
    public services?: Service[];

    constructor(tagName: string) {
        super();
        this.tagName = tagName;
    }
}
