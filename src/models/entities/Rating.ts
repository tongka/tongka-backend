import { BaseEntity, Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { Service } from "./Service";
import { User } from "./User";

@Entity()
export class Rating extends BaseEntity {
    @ManyToOne(type => User, user => user.id, { primary: true })
    @JoinColumn({ name: "user_id" })
    public user?: User;

    @ManyToOne(type => Service, service => service.id, { primary: true })
    @JoinColumn({ name: "service_id" })
    public service?: Service;

    @Column("smallint")
    public score: number;

    constructor(user: User, service: Service, score: number) {
        super();
        this.user = user;
        this.service = service;
        this.score = score;
    }
}
