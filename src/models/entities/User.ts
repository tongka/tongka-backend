import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { PaymentMethod } from "../payment/pay";
import { Photo } from "./Photo";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn("increment")
    public readonly id?: number;

    @Column("varchar", { length: 100, unique: true })
    public email: string;

    @Column("varchar", { length: 100, select: false })
    public password: string;

    @Column("varchar", { length: 50 })
    public firstname: string;

    @Column("varchar", { length: 50 })
    public lastname: string;

    @Column("varchar", { length: 15 })
    public type: "photographer" | "customer";

    @Column("text", { nullable: true })
    public bio?: string;

    @Column("date", { name: "birthday", nullable: true })
    public birthday?: Date;

    @Column("varchar", { nullable: true, length: 10 })
    public phone?: string;

    @Column("varchar", { nullable: true, length: 100 })
    public facebook?: string;

    @Column("varchar", { nullable: true, length: 100 })
    public instagram?: string;

    @Column("varchar", { name: "line_id", nullable: true, length: 100 })
    public lineId?: string;

    @Column("datetime", { name: "joined_date" })
    public joinedDate: Date;

    @Column("int", { name: "derived_prev_event_count" })
    public eventCount: number;

    @Column("double", { name: "derived_prev_review_count" })
    public reviewCount: number;

    @Column("double", { nullable: true })
    public rating?: number;

    @Column("varchar", { name: "payment_type", length: 15 })
    public paymentType: PaymentMethod;

    @Column("text", { name: "bank_account_number", nullable: true })
    public bankAccountNumber?: string;

    @Column("text", { name: "credit_card_number", nullable: true })
    public creditCardNumber?: string;

    @Column("date", { name: "card_expire_date", nullable: true })
    public cardExpireDate?: Date;

    @Column("varchar", { nullable: true, length: 10 })
    public cvv?: string;

    @Column("tinyint", { nullable: false, default: 1, width: 1 })
    public isCreditCardAvailiable: 0 | 1;

    @OneToOne(type => Photo, photo => photo.id)
    @JoinColumn({ name: "profile_photo_id" })
    public profilePhoto?: Photo;

    @OneToOne(type => Photo, photo => photo.id)
    @JoinColumn({ name: "cover_photo_id" })
    public coverPhoto?: Photo;

    // @OneToMany(type => Photo, { onDelete: "CASCADE" })
    // @JoinTable({
    //     name: "portfolio",
    //     joinColumns: [{ name: "user_id" }],
    //     inverseJoinColumns: [{ name: "photo_id" }]
    // })
    // public portfolioPhotos?: Photo[];

    constructor(email: string, password: string, firstname: string, lastname: string,
        type: "photographer" | "customer", paymentType: PaymentMethod) {
        super();
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.type = type;
        this.joinedDate = new Date();
        this.eventCount = 0;
        this.reviewCount = 0;
        this.paymentType = paymentType;
        this.isCreditCardAvailiable = 0;
    }
}
