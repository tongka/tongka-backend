import {
    BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn
} from "typeorm";
import { PaymentMethod } from "../payment/pay";
import { Photo } from "./Photo";
import { Service } from "./Service";
import { User } from "./User";

@Entity()
export class Event extends BaseEntity {
    @PrimaryGeneratedColumn("increment")
    public readonly id?: number;

    @Column("double")
    public price: number;

    @Column("varchar", { length: 15 })
    public type: "halfday" | "fullday";

    @Column("datetime", { name: "event_date" })
    public eventDate: Date;

    @Column("timestamp", { name: "request_date", default: () => "CURRENT_TIMESTAMP" })
    public requestDate?: Date;

    @Column("timestamp", { name: "photo_deliver_date", nullable: true })
    public deliverDate?: Date;

    @Column("double", { name: "paid_amount" })
    public paidAmount: number;

    @Column("text", { name: "photos_link", nullable: true })
    public photosLink?: string;

    @Column("text", { nullable: true })
    public location: string;

    @Column("text", { nullable: true })
    public description?: string;

    @Column("varchar", { length: 20, name: "deposit_payment_method", nullable: true })
    public depositPaymentMethod?: PaymentMethod;

    @OneToOne(type => Photo, photo => photo.id)
    @JoinColumn({ name: "deposit_slip_id" })
    public depositSlip?: Photo;

    @Column("varchar", { length: 20, name: "total_payment_method", nullable: true })
    public totalPaymentMethod?: PaymentMethod;

    @OneToOne(type => Photo, photo => photo.id)
    @JoinColumn({ name: "total_slip_id" })
    public totalSlip?: Photo;

    @Column("varchar", { length: 20, name: "redeem_payment_method", nullable: true })
    public redeemPaymentMethod?: PaymentMethod;

    @OneToOne(type => Photo, photo => photo.id)
    @JoinColumn({ name: "redeem_slip_id" })
    public redeemSlip?: Photo;

    @Column("tinyint", { name: "cancel", width: 1, default: () => 0 })
    public isCancel: 0 | 1;

    @Column("tinyint", { name: "close", width: 1, default: () => 0 })
    public isClose: 0 | 1;

    @Column("int")
    public state: number;

    @ManyToOne(type => Service, { onDelete: "CASCADE" })
    @JoinColumn({ name: "service_id" })
    public service: Service;

    @ManyToOne(type => User, { onDelete: "CASCADE" })
    @JoinColumn({ name: "customer_id" })
    public customer: User;

    constructor(service: Service, customer: User, eventDate: Date, price: number, location: string,
        type: "halfday" | "fullday") {
        super();
        this.eventDate = eventDate;
        this.type = type;
        this.paidAmount = 0;
        this.isCancel = 0;
        this.isClose = 0;
        this.state = EventState.REQUESTED;
        this.price = price;
        this.location = location;
        this.service = service;
        this.customer = customer;
    }
}

export const EventState = {
    REQUESTED: 0,
    ACCEPTED: 1,
    DEPOSIT_PAID: 2,
    DEPOSIT_ACCEPTED: 3,
    PHOTOS_READY: 4,
    TOTAL_PAID: 5,
    COMPLETED: 6,
    REDEEM_CANCELLED: 7,
    NO_REDEEM_CANCELLED: 8,
    DEPOSIT_RETURNED: 9,
    DEPOSIT_RETURN_ACCEPTED: 10
};
