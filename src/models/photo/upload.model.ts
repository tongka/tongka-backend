import { File } from "formidable";
import path from "path";
import sharp from "sharp";
import { getRepository } from "typeorm";
import { Photo } from "../entities/Photo";
import { User } from "../entities/User";

const generateRandomId = async (): Promise<string> => {
    while (true) {
        const random = Math.random().toString(36).substring(2, 14);
        const photo = await getRepository(Photo).findOne({ id: random });
        if (!photo) {
            return random;
        }
    }
};

const insertPhotoDatabase = async (id: string, filepath: string, owner: User, showPortfolio: 0 | 1): Promise<Photo> => {
    const photo = new Photo(id, filepath, owner, showPortfolio);
    return await photo.save();
};

export const uploadPhoto = async (file: File, owner: User, transformer: sharp.Sharp, showPortfolio: 0 | 1 = 0)
    : Promise<Photo> => {
    const photoId = await generateRandomId();
    const destFileName = "uploads/" + photoId + ".jpg";
    const destFilePath = path.resolve(__dirname, "../../..", destFileName);
    await sharp(file.path).pipe(transformer.jpeg()).toFile(destFilePath);
    return await insertPhotoDatabase(photoId, destFileName, owner, showPortfolio);
};
