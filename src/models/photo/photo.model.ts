import { getRepository } from "typeorm";
import { Photo } from "../entities/Photo";

export const getPhotoById = async (id: string): Promise<Photo | undefined> => {
    const photoRepository = await getRepository(Photo);
    return await photoRepository.findOne({
        where: { id },
        relations: ["owner"]
    });
};

export const getUserPortfolio = async (userId: number): Promise<Photo[]> => {
    const photoRepository = await getRepository(Photo);
    return await photoRepository.find({
        where: {
            owner: userId,
            showPortfolio: 1
        }
    });
};

export const removePortfolio = async (photoId: string) => {
    const photoRepository = await getRepository(Photo);
    await photoRepository.delete(photoId);
};
