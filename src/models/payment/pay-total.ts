import { File } from "formidable";
import sharp from "sharp";
import { Event, EventState } from "../entities/Event";
import { uploadPhoto } from "../photo/upload.model";
import { payByCreditCard } from "./pay";
import { verifyUserCreditCard } from "./verify";

export const payTotalCreditCard = async (event: Event): Promise<Event> => {
    await verifyUserCreditCard(event.customer);
    const creditCardNumber = event.customer.creditCardNumber!;
    const cvv = event.customer.cvv!;
    const bankAccountNumber = event.service.photographer!.bankAccountNumber!;
    const payAmount = event.price - event.paidAmount;
    const payment = await payByCreditCard(creditCardNumber, cvv, bankAccountNumber, payAmount);
    event.state = EventState.TOTAL_PAID;
    event.totalPaymentMethod = "credit card";
    return await event.save();
};

export const payTotalBankAccount = async (event: Event, slipFile: File): Promise<Event> => {
    const slipTransformer = sharp().resize(1920);
    const photo = await uploadPhoto(slipFile, event.customer, slipTransformer);
    event.state = EventState.TOTAL_PAID;
    event.totalPaymentMethod = "bank account";
    event.totalSlip = photo;
    return await event.save();
};
