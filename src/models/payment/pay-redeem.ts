import { File } from "formidable";
import sharp from "sharp";
import { Event, EventState } from "../entities/Event";
import { uploadPhoto } from "../photo/upload.model";
import { payByCreditCard } from "./pay";
import { verifyUserCreditCard } from "./verify";

export const payRedeemCreditCard = async (event: Event): Promise<Event> => {
    await verifyUserCreditCard(event.service.photographer!);
    const creditCardNumber = event.service.photographer!.creditCardNumber!;
    const cvv = event.service.photographer!.cvv!;
    const bankAccountNumber = event.customer.bankAccountNumber!;
    const payAmount = event.paidAmount;
    const payment = await payByCreditCard(creditCardNumber, cvv, bankAccountNumber, payAmount);
    event.state = EventState.DEPOSIT_RETURNED;
    event.redeemPaymentMethod = "credit card";
    return await event.save();
};

export const payRedeemBankAccount = async (event: Event, slipFile: File): Promise<Event> => {
    const slipTransformer = sharp().resize(1920);
    const photo = await uploadPhoto(slipFile, event.customer, slipTransformer);
    event.state = EventState.DEPOSIT_RETURNED;
    event.redeemPaymentMethod = "bank account";
    event.redeemSlip = photo;
    return await event.save();
};
