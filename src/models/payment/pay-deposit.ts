import { File } from "formidable";
import sharp from "sharp";
import { Event, EventState } from "../entities/Event";
import { uploadPhoto } from "../photo/upload.model";
import { payByCreditCard } from "./pay";
import { verifyUserCreditCard } from "./verify";

export const payDepositCreditCard = async (event: Event): Promise<Event> => {
    await verifyUserCreditCard(event.customer);
    const creditCardNumber = event.customer.creditCardNumber!;
    const cvv = event.customer.cvv!;
    const bankAccountNumber = event.service.photographer!.bankAccountNumber!;
    const payAmount = event.price * 0.3;
    const payment = await payByCreditCard(creditCardNumber, cvv, bankAccountNumber, payAmount);
    event.state = EventState.DEPOSIT_PAID;
    event.depositPaymentMethod = "credit card";
    return await event.save();
};

export const payDepositBankAccount = async (event: Event, slipFile: File): Promise<Event> => {
    const slipTransformer = sharp().resize(1920);
    const photo = await uploadPhoto(slipFile, event.customer, slipTransformer);
    event.state = EventState.DEPOSIT_PAID;
    event.depositPaymentMethod = "bank account";
    event.depositSlip = photo;
    return await event.save();
};
