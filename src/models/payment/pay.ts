import axios from "axios";
import { bankBaseUrl } from "../../config";

export type PaymentMethod = "bank account" | "credit card";

export const payByCreditCard = async (fromCard: string, cvv: string, toAccount: string, amount: number) => {
    try {
        const response = await axios.post(bankBaseUrl + "/pay", {
            cardNumber: fromCard, cvv,
            accountNo: toAccount,
            amount
        });
        return response.data;
    } catch (e) {
        throw new Error("Failed to pay by credit card");
    }
};
