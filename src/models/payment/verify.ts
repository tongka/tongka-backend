import axios from "axios";
import { bankBaseUrl } from "../../config";
import { User } from "../entities/User";

export const verifyCreditCard = async (cardNumber: string, cvv: string): Promise<number> => {
    cardNumber = cardNumber.replace(/\-| /g, "");
    if (!cardNumber.match(/[0-9]{16}/) || !cvv.match(/[0-9]{3}/)) {
        return 0;
    }
    try {
        const response = await axios.post(bankBaseUrl + "/card/validate", { cardNumber, cvv });
        return response.data.valid;
    } catch (e) {
        throw new Error("Unable to verify credit card");
    }
};

export const verifyUserCreditCard = async (user: User): Promise<void> => {
    if (!user.creditCardNumber || !user.cvv || !user.cardExpireDate) {
        throw new Error("No credit card information provided");
    }
    if (new Date() >= user.cardExpireDate) {
        throw new Error("Credit card expired");
    }
    if (!await verifyCreditCard(user.creditCardNumber, user.cvv)) {
        throw new Error("Credit card information is invalid");
    }
};
