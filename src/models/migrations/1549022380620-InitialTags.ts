import { getRepository, MigrationInterface, QueryRunner } from "typeorm";
import { Tag } from "../entities/Tag";

export class InitialTags1549022380620 implements MigrationInterface {

    private tagNames = [
        "งานรับปริญญา", "ภาพ portrait",
        "งานแต่งงาน", "งานพรีเวดดิ้ง",
        "งานอีเว้น", "สถาปัตยกรรม",
        "สินค้า หรือ อาหาร", "อื่นๆ",
    ];

    public async up(queryRunner: QueryRunner): Promise<any> {
        this.tagNames.map(name => new Tag(name)).forEach(async tag => await tag.save());
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        this.tagNames.forEach(async name => await getRepository(Tag).delete({ tagName: name }));
    }

}
