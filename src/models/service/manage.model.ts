import { getRepository } from "typeorm";
import { getService } from "../../models/service/get.model";
import { Service } from "../entities/Service";
import { getTags } from "./tag.model";

export const editService = async (id: number, title: string, openDate?: string, closeDate?: string,
    halfDayPrice?: number, fullDayPrice?: number, description?: string, location?: string, tags?: string[]) => {
    let service = await getService(id);
    if (!service) {
        return undefined;
    }
    if (title) { service.title = title; }
    if (openDate) { service.openDate = new Date(openDate); }
    if (closeDate) { service.closeDate = new Date(closeDate); }
    if (halfDayPrice) { service.halfDayPrice = halfDayPrice; }
    if (fullDayPrice) { service.fullDayPrice = fullDayPrice; }
    if (description) { service.description = description; }
    if (location) { service.location = location; }
    if (tags) { service.tags = await getTags(tags); }
    service = await service.save();
    return service;
};

export const deleteService = async (id: number) => {
    const repository = await getRepository(Service);
    await repository.delete(id);
};
