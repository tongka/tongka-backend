import { getRepository } from "typeorm";
import { Service } from "../entities/Service";

export const getService = async (id: number): Promise<Service | undefined> => {
    return await getRepository(Service).findOne(id, {
        relations: ["photographer", "photographer.profilePhoto", "tags", "coverPhoto"]
    });
};

export const getPhotographerServices = async (userId: number): Promise<Service[]> => {
    return await getRepository(Service).find({
        where: {
            photographer: userId
        },
        relations: ["tags", "coverPhoto"]
    });
};
