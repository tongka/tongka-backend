import { getRepository, Like } from "typeorm";
import { Service } from "../entities/Service";

export const searchService = async (q: string): Promise<Service[]> => {
    const services: Service[] = await getRepository(Service).find({
        where: { title: Like("%" + q + "%") },
        relations: ["photographer", "photographer.profilePhoto", "tags", "coverPhoto"]
    });
    return services;
};
