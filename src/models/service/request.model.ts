import { Event } from "../entities/Event";
import { getProfile } from "../user/get-profile.model";
import { getService } from "./get.model";

export const serviceRequest = async (serviceId: number, customerId: number, eventDate: string,
    price: string, location: string, description: string, type: "halfday" | "fullday") => {
    const service = await getService(serviceId);
    if (!service) { return undefined; }
    const customer = await getProfile(customerId);
    if (!customer) { return undefined; }
    let event = new Event(service, customer, new Date(eventDate), parseFloat(price), location, type);
    if (description) { event.description = description; }
    event = await event.save();
    return event;
};
