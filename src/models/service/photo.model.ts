import { File } from "formidable";
import sharp = require("sharp");
import { Service } from "../entities/Service";
import { uploadPhoto } from "../photo/upload.model";
import { getService } from "./get.model";

export const changeServicePhoto = async (serviceId: number, file: File): Promise<Service | undefined> => {
    const transformer = sharp().resize(2560);
    const service = await getService(serviceId);
    if (!service) {
        return undefined;
    }
    const photo = await uploadPhoto(file, service.photographer!, transformer);
    delete photo.owner;
    service.coverPhoto = photo;
    return await service.save();
};
