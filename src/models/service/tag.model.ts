import _ from "lodash";
import { getRepository } from "typeorm";
import { Tag } from "../entities/Tag";

export const getTag = async (tagId: number): Promise<Tag | undefined> => {
    return await getRepository(Tag).findOne({ id: tagId });
};

export const getTags = async (tagStrings: string[]): Promise<Tag[]> => {
    // might change to query builder
    const tags = _.compact(await Promise.all(
        tagStrings.map(tagId => getTag(parseInt(tagId, 10)))
    ));
    return tags;
};

export const getAllTags = async (): Promise<Tag[]> => {
    return await Tag.find();
};
