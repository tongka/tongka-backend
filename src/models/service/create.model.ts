import { Service } from "../entities/Service";
import { User } from "../entities/User";
import { getTags } from "./tag.model";

export const serviceCreate = async (photographer: User, title: string, openDate?: string, closeDate?: string,
    halfDayPrice?: number, fullDayPrice?: number, description?: string, location?: string, tags?: string[]) => {
    let service = new Service(photographer, title);
    if (openDate) { service.openDate = new Date(openDate); }
    if (closeDate) { service.closeDate = new Date(closeDate); }
    if (halfDayPrice) { service.halfDayPrice = halfDayPrice; }
    if (fullDayPrice) { service.fullDayPrice = fullDayPrice; }
    if (description) { service.description = description; }
    if (location) { service.location = location; }
    service = await service.save();
    if (tags) { service.tags = await getTags(tags); }
    service = await service.save();
    return service;
};
