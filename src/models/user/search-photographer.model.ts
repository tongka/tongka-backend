import { getRepository, Like } from "typeorm";
import { User } from "../entities/User";

export const searchPhotographer = async (q: string): Promise<User[]> => {
    const photographers: User[] = await getRepository(User).find({
        where: {
            firstname: Like("%" + q + "%"),
            type: "photographer"
        },
        relations: [
            "profilePhoto"
        ]
    });
    return photographers;
};
