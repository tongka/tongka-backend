import { getRepository } from "typeorm";
import { User } from "../entities/User";

export const deleteUser = async (id: number) => {
    const repository = await getRepository(User);
    await repository.delete(id);
};
