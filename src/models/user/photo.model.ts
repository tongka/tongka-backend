import { File } from "formidable";
import sharp from "sharp";
import { User } from "../entities/User";
import { uploadPhoto } from "../photo/upload.model";
import { getProfile } from "./get-profile.model";

export const changeProfilePhoto = async (userId: number, file: File): Promise<User | undefined> => {
    const transformer = sharp().resize(512, 512, { position: sharp.strategy.entropy });
    const user = await getProfile(userId);
    if (!user) {
        return undefined;
    }
    const photo = await uploadPhoto(file, user, transformer);
    delete photo.owner;
    user.profilePhoto = photo;
    return await user.save();
};

export const changeCoverPhoto = async (userId: number, file: File): Promise<User | undefined> => {
    const transformer = sharp().resize(2560);
    const user = await getProfile(userId);
    if (!user) {
        return undefined;
    }
    const photo = await uploadPhoto(file, user, transformer);
    delete photo.owner;
    user.coverPhoto = photo;
    return await user.save();
};
