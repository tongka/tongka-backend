import { getRepository } from "typeorm";
import { User } from "../entities/User";

export const getProfile = async (id: number): Promise<User | undefined> => {
    return await getRepository(User).findOne(id, {
        relations: ["profilePhoto", "coverPhoto"]
    });
};
