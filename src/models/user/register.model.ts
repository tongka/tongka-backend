import { createHash } from "crypto";
import { getRepository } from "typeorm";
import { User } from "../entities/User";
import { PaymentMethod } from "../payment/pay";

export const hashPassword = (password: string): string => {
    const hash = createHash("sha1");
    hash.update(password);
    return hash.digest("hex");
};

const isEmailAvailable = async (email: string): Promise<boolean> => {
    const user = await getRepository(User).findOne({
        where: { email }
    });
    return user === undefined;
};

export const userRegister = async (email: string, password: string, firstname: string, lastname: string,
    type: "photographer" | "customer", paymentType: PaymentMethod, bankAccountNumber?: string,
    creditCardNumber?: string, cardExpireDate?: Date, cvv?: string): Promise<string | undefined> => {
    if (!await isEmailAvailable(email)) {
        return "This email is not available";
    }
    const user = new User(email, hashPassword(password), firstname, lastname, type, paymentType);
    if (!bankAccountNumber) { return "Bank account number is missing"; }
    // if(verify and validate){...}
    user.bankAccountNumber = bankAccountNumber;
    if (creditCardNumber && cardExpireDate && cvv) {
        // if(verify and validate){...}
        user.creditCardNumber = creditCardNumber;
        user.cardExpireDate = cardExpireDate;
        user.cvv = cvv;
        user.isCreditCardAvailiable = 1;
    }
    if (paymentType === "credit card" && !(creditCardNumber && cardExpireDate && cvv)) {
        return "Payment information is missing for payment by card";
    }
    await user.save();
    return undefined;
};
