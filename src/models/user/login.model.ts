import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { jwtKey } from "../../config";
import { User } from "../entities/User";
import { hashPassword } from "./register.model";

export const userLogin = async (email: string, password: string): Promise<string | undefined> => {
    const user = await getRepository(User).findOne({
        select: ["id", "password"],
        where: { email }
    });
    if (!user || user.password !== hashPassword(password)) {
        return undefined;
    }
    const accessToken = jwt.sign({
        userId: user.id
    }, jwtKey);
    return accessToken;
};

export const authorizeUser = async (id: number, password: string): Promise<boolean> => {
    const user = await getRepository(User).findOne({
        select: ["password"],
        where: { id }
    });
    if (user && user.password === hashPassword(password)) {
        return true;
    }
    return false;
};
