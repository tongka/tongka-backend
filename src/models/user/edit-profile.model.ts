import _ from "lodash";
import { getRepository } from "typeorm";
import { User } from "../entities/User";
import { PaymentMethod } from "../payment/pay";
import { getProfile } from "./get-profile.model";

export const editProfile = async (userId: number, firstname?: string, lastname?: string, bio?: string,
    birthday?: Date, phone?: string, facebook?: string, instagram?: string, lineId?: string,
    paymentType?: PaymentMethod, bankAccountNumber?: string, creditCardNumber?: string,
    cardExpireDate?: Date, cvv?: string): Promise<User | undefined> => {
    const setObject = _.pickBy({ firstname, lastname, bio, birthday, phone, facebook, instagram, lineId });
    if (Object.keys(setObject).length > 0) {
        await getRepository(User).createQueryBuilder("user").update().set(setObject)
            .where("id = :id", { id: userId }).execute();
    }
    let user = await getProfile(userId);
    if (!user) { return undefined; }
    if (paymentType === "bank account") { user.paymentType = paymentType; }
    if (bankAccountNumber) {
        user.bankAccountNumber = bankAccountNumber;
        // if(verify and validate){...} bank account
    }
    if (creditCardNumber) { user.creditCardNumber = creditCardNumber; }
    if (cardExpireDate) { user.cardExpireDate = new Date(cardExpireDate); }
    if (cvv) { user.cvv = cvv; }
    if (user.creditCardNumber && user.cardExpireDate && user.cvv) { user.isCreditCardAvailiable = 1; }
    // if(verify and validate){...} credit card
    if (paymentType === "credit card") {
        if (!(user.creditCardNumber && user.cardExpireDate && user.cvv)) {
            user.isCreditCardAvailiable = 0;
            return undefined;
        }
        if (paymentType) { user.paymentType = paymentType; }
    }
    user = await user.save();
    return user;
};
