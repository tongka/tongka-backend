import cors from "@koa/cors";
import Koa from "koa";
import bodyParser from "koa-body";
import mount from "koa-mount";
import serve from "koa-static";
import { createConnection } from "typeorm";
import { dbConfig, serverPort } from "./config";
import { logger } from "./controllers/middlewares";
import router from "./routes/router";

(async () => {
    await createConnection(dbConfig);
    const app = new Koa();
    app.use(cors());
    app.use(logger);
    app.use(mount("/uploads", serve("uploads")));
    app.use(bodyParser({ multipart: true }));
    app.use(router.routes());
    app.listen(serverPort, () => {
        console.log("Server started at port " + serverPort);
    });
})();
