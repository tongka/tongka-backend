import { IsNotEmpty, IsNumberString, IsUrl, validate } from "class-validator";
import { Context } from "koa";
import { deliverPhoto } from "../../models/event/deliver-photo.model";
import { respondError, respondSuccess } from "../response";

class DeliverPhotoForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    @IsNotEmpty()
    @IsUrl()
    public photoLink: string;

    constructor(body: any) {
        this.id = body.id;
        this.photoLink = body.photoLink;
    }
}

export const deliverPhotoController = async (ctx: Context) => {
    const form = new DeliverPhotoForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const event = await deliverPhoto(parseInt(form.id, 10), form.photoLink);
    if (!event) {
        return respondError(ctx, ["Photo deliver error."]);
    }
    respondSuccess(ctx, event);
};
