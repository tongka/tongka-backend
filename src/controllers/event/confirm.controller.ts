import { IsIn, IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { acceptEvent, rejectEvent } from "../../models/event/confirm.model";
import { respondError, respondSuccess } from "../response";

class ConfirmEventForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    @IsNumberString()
    @IsIn(["0", "1"])
    public accept: string;

    constructor(body: any) {
        this.id = body.id;
        this.accept = body.accept;
    }
}

export const confirmEventController = async (ctx: Context) => {
    const form = new ConfirmEventForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    let confirmation;
    if (form.accept === "1") {
        confirmation = await acceptEvent(parseInt(form.id, 10));
    } else {
        confirmation = await rejectEvent(parseInt(form.id, 10));
    }
    if (!confirmation) {
        return respondError(ctx, ["Event confirmation error."]);
    }
    respondSuccess(ctx, { accept: form.accept });
};
