import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { cancelEvent } from "../../models/event/cancel.model";
import { respondError, respondSuccess } from "../response";

class CancelEventForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const cancelEventController = async (ctx: Context) => {
    const form = new CancelEventForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const event = await cancelEvent(parseInt(form.id, 10), ctx.state.user);
    if (!event) { return respondError(ctx, ["Cancellation error."]); }
    respondSuccess(ctx, event);
};
