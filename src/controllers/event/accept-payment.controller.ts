import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { acceptPayment, acceptRedeem } from "../../models/event/accept-payment.model";
import { respondError, respondSuccess } from "../response";

class AcceptPaymentForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const acceptPaymentController = async (ctx: Context) => {
    const form = new AcceptPaymentForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const eventSuccess = await acceptPayment(parseInt(form.id, 10));
    if (!eventSuccess) {
        return respondError(ctx, ["Payment accept error."]);
    }
    respondSuccess(ctx, { accept: "1" });
};

export const acceptRedeemController = async (ctx: Context) => {
    const form = new AcceptPaymentForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const eventSuccess = await acceptRedeem(parseInt(form.id, 10));
    if (!eventSuccess) {
        return respondError(ctx, ["Redeem accept error."]);
    }
    respondSuccess(ctx, { accept: "1" });
};
