import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { getEventById, getGroupedEvents } from "../../models/event/get.model";
import { respondError, respondSuccess } from "../response";

class GetEventForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const getMyEventsController = async (ctx: Context) => {
    const event = await getGroupedEvents(ctx.state.user.id);
    respondSuccess(ctx, event);
};

export const getEventController = async (ctx: Context) => {
    const form = new GetEventForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const event = await getEventById(parseInt(form.id, 10));
    if (!event) {
        return respondError(ctx, ["Event not found."]);
    }
    respondSuccess(ctx, event);
};
