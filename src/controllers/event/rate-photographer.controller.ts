import { IsIn, IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { ratePhotographer } from "../../models/event/rate-photographer.model";
import { respondError, respondSuccess } from "../response";

class RatePhotographerForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    @IsIn(["1", "2", "3", "4", "5"])
    public score: string;

    constructor(body: any) {
        this.id = body.id;
        this.score = body.score;
    }
}

export const ratePhotographerController = async (ctx: Context) => {
    const form = new RatePhotographerForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const rating = await ratePhotographer(parseInt(form.id, 10), parseInt(form.score, 10));
    if (!rating) {
        return respondError(ctx, ["Failed to rate photographer."]);
    }
    respondSuccess(ctx, { score: rating.score });
};
