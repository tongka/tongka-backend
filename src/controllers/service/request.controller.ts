import { IsDateString, IsIn, IsNotEmpty, IsNumberString, MinLength, validate } from "class-validator";
import { Context } from "koa";
import { serviceRequest } from "../../models/service/request.model";
import { respondError, respondSuccess } from "../response";

class ServiceRequestForm {
    @IsNotEmpty()
    public serviceId: number;

    // "2017-06-07T14:34:08.700Z", "2017-06-07T14:34:08.700" , "2017-06-07T14:34:08+04:00"
    @IsNotEmpty()
    @IsDateString()
    public eventDate: string;

    @IsNotEmpty()
    @IsNumberString()
    public price: string;

    @MinLength(1)
    public location: string;

    public description: string;

    @IsIn(["halfday", "fullday"])
    public type: string;

    constructor(body: any) {
        this.serviceId = body.serviceId;
        this.eventDate = body.eventDate;
        this.price = body.price;
        this.type = body.type;
        this.location = body.location;
        this.description = body.description;
    }
}

export const serviceRequestController = async (ctx: Context) => {
    const form = new ServiceRequestForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const service = await serviceRequest(form.serviceId, ctx.state.user.id, form.eventDate,
        form.price, form.location, form.description, form.type as ("halfday" | "fullday"));
    respondSuccess(ctx, service);
};
