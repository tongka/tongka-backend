import { IsArray, IsDateString, IsNumberString, IsOptional, Length, validate } from "class-validator";
import { Context } from "koa";
import { serviceCreate } from "../../models/service/create.model";
import { respondError, respondSuccess } from "../response";

class ServiceCreateForm {
    @Length(1, 100)
    public title: string;

    // Checks if a string is a complete representation of a date
    // "2017-06-07T14:34:08.700Z", "2017-06-07T14:34:08.700" , "2017-06-07T14:34:08+04:00"
    @IsOptional()
    @IsDateString()
    public openDate: string;

    // Checks if a string is a complete representation of a date
    // "2017-06-07T14:34:08.700Z", "2017-06-07T14:34:08.700" , "2017-06-07T14:34:08+04:00"
    @IsOptional()
    @IsDateString()
    public closeDate: string;

    @IsOptional()
    @IsNumberString()
    public halfDayPrice: string;

    @IsOptional()
    @IsNumberString()
    public fullDayPrice: string;

    @IsOptional()
    @Length(1)
    public description: string;

    @IsOptional()
    @Length(1)
    public location: string;

    @IsOptional()
    @IsArray()
    public tags: string[];

    constructor(body: any) {
        this.title = body.title;
        this.openDate = body.openDate;
        this.closeDate = body.closeDate;
        this.halfDayPrice = body.halfDayPrice;
        this.fullDayPrice = body.fullDayPrice;
        this.description = body.description;
        this.tags = body.tags;
        this.location = body.location;
    }
}

export const serviceCreateController = async (ctx: Context) => {
    const form = new ServiceCreateForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const service = await serviceCreate(ctx.state.user, form.title, form.openDate, form.closeDate,
        parseFloat(form.halfDayPrice), parseFloat(form.fullDayPrice), form.description, form.location, form.tags);
    respondSuccess(ctx, service);
};
