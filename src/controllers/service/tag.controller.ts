import { Context } from "koa";
import { getAllTags } from "../../models/service/tag.model";
import { respondSuccess } from "../response";

export const getAllTagsController = async (ctx: Context) => {
    return respondSuccess(ctx, await getAllTags());
};
