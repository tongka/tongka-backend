import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { getPhotographerServices, getService } from "../../models/service/get.model";
import { respondError, respondSuccess } from "../response";

class GetServiceForm {
    @IsNotEmpty()
    public id: number;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const getServiceController = async (ctx: Context) => {
    const form = new GetServiceForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const service = await getService(form.id);
    if (!service) {
        return respondError(ctx, ["Service not found."]);
    }
    respondSuccess(ctx, service);
};

export const getPhotographerServicesController = async (ctx: Context) => {
    const services = await getPhotographerServices(ctx.state.user.id);
    respondSuccess(ctx, services);
};
