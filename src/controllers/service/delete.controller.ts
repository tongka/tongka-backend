import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { getService } from "../../models/service/get.model";
import { deleteService } from "../../models/service/manage.model";
import { respondError, respondSuccess } from "../response";

class DeleteServiceForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const serviceDeleteController = async (ctx: Context) => {
    const form = new DeleteServiceForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const service = await getService(parseInt(form.id, 10));
    if (!service) {
        return respondError(ctx, ["Service not found"]);
    }
    await deleteService(parseInt(form.id, 10));
    respondSuccess(ctx);
};
