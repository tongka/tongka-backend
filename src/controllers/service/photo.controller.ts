import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { File } from "formidable";
import { Context } from "koa";
import { getService } from "../../models/service/get.model";
import { changeServicePhoto } from "../../models/service/photo.model";
import { respondError, respondForbidden, respondSuccess } from "../response";

class PostServicePhotoForm {
    @IsNotEmpty()
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const postServicePhotoController = async (ctx: Context) => {
    const form = new PostServicePhotoForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    if (!ctx.request.files || !ctx.request.files.imageFile) {
        return respondError(ctx, ["File not found"]);
    }
    const imageFile: File = ctx.request.files.imageFile;
    if (!imageFile.type.match(/image\/*/)) {
        return respondError(ctx, ["Not an image file"]);
    }
    let service = await getService(parseInt(form.id, 10));
    if (!service) {
        return respondError(ctx, ["Service not found"]);
    }
    if (service.photographer!.id !== ctx.state.user.id) {
        return respondForbidden(ctx);
    }
    service = await changeServicePhoto(parseInt(form.id, 10), imageFile);
    respondSuccess(ctx, service);
};
