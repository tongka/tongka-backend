import { IsArray, IsDateString, IsNotEmpty, IsNumberString, IsOptional, Length, validate } from "class-validator";
import { Context } from "koa";
import { editService } from "../../models/service/manage.model";
import { respondError, respondSuccess } from "../response";

class ServiceEditForm {
    @IsNotEmpty()
    public id: number;

    @IsOptional()
    @Length(1, 100)
    public title: string;

    @IsOptional()
    @IsDateString()
    public openDate: string;

    @IsOptional()
    @IsDateString()
    public closeDate: string;

    @IsOptional()
    @IsNumberString()
    public halfDayPrice: string;

    @IsOptional()
    @IsNumberString()
    public fullDayPrice: string;

    @IsOptional()
    @Length(1)
    public description: string;

    @IsOptional()
    @Length(1)
    public location: string;

    @IsOptional()
    @IsArray()
    public tags: string[];

    constructor(body: any) {
        this.id = body.id;
        this.title = body.title;
        this.openDate = body.openDate;
        this.closeDate = body.closeDate;
        this.halfDayPrice = body.halfDayPrice;
        this.fullDayPrice = body.fullDayPrice;
        this.description = body.description;
        this.tags = body.tags;
        this.location = body.location;
    }
}

export const serviceEditController = async (ctx: Context) => {
    const form = new ServiceEditForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const service = await editService(form.id, form.title, form.openDate, form.closeDate,
        parseFloat(form.halfDayPrice), parseFloat(form.fullDayPrice), form.description, form.location, form.tags);
    respondSuccess(ctx, service);
};
