import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { searchService } from "../../models/service/search.model";
import { respondError, respondSuccess } from "../response";

class SearchServiceForm {
    @IsNotEmpty()
    public q: string;

    constructor(body: any) {
        this.q = body.q;
    }
}

export const searchServiceController = async (ctx: Context) => {
    const form = new SearchServiceForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const services = await searchService(form.q);
    respondSuccess(ctx, services);
};
