import { Context } from "koa";

export function respondSuccess(ctx: Context, data?: any) {
    ctx.status = 200;
    ctx.body = data || { status: 1 };
}

export function respondError(ctx: Context, errors: string[]) {
    ctx.status = 400;
    ctx.body = errors;
}

export function respondUnauthorized(ctx: Context) {
    ctx.status = 401;
    ctx.body = ["Unauthorized"];
}

export function respondForbidden(ctx: Context) {
    ctx.status = 403;
    ctx.body = ["Forbidden"];
}

export function respondInternalError(ctx: Context) {
    ctx.status = 500;
    ctx.body = ["Internal Server Error"];
}
