import { Length, validate } from "class-validator";
import { Context } from "koa";
import { verifyCreditCard } from "../../models/payment/verify";
import { respondError, respondSuccess } from "../response";

class VerifyCreditCardForm {
    @Length(16, 19)
    public cardNumber: string;

    @Length(3, 3)
    public cvv: string;

    constructor(body: any) {
        this.cardNumber = body.cardNumber;
        this.cvv = body.cvv;
    }
}

export const verifyCreditCardController = async (ctx: Context) => {
    const form = new VerifyCreditCardForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const valid = await verifyCreditCard(form.cardNumber, form.cvv);
    return respondSuccess(ctx, { valid });
};
