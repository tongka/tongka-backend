import { IsIn, IsNumberString, IsOptional, validate } from "class-validator";
import { Context } from "koa";
import { EventState } from "../../models/entities/Event";
import { User } from "../../models/entities/User";
import { getEventById } from "../../models/event/get.model";
import { PaymentMethod } from "../../models/payment/pay";
import { payTotalBankAccount, payTotalCreditCard } from "../../models/payment/pay-total";
import { respondError, respondSuccess } from "../response";

class PayTotalForm {
    @IsNumberString()
    public id: string;

    @IsOptional()
    @IsIn(["bank account", "credit card"])
    public method: PaymentMethod;

    constructor(body: any) {
        this.id = body.id;
        this.method = body.method;
    }
}

export const payTotalController = async (ctx: Context) => {
    const form = new PayTotalForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const user: User = ctx.state.user;
    if (form.method === "credit card" && !user.isCreditCardAvailiable) {
        return respondError(ctx, ["Credit card information not available"]);
    } else if (form.method === "bank account" && !user.bankAccountNumber) {
        return respondError(ctx, ["Bank account information not available"]);
    }
    form.method = form.method || user.paymentType;
    let event = await getEventById(parseInt(form.id, 10));
    try {
        if (!event) { throw new Error("Event not found"); }
        if (event.state !== EventState.PHOTOS_READY) { throw new Error("Not in an accepted state"); }
        if (form.method === "bank account") {
            if (!ctx.request.files || !ctx.request.files.slipFile) {
                throw new Error("Slip file not attached");
            }
            event = await payTotalBankAccount(event, ctx.request.files.slipFile);
        } else {
            event = await payTotalCreditCard(event);
        }
        respondSuccess(ctx, event);
    } catch (e) {
        respondError(ctx, [e.message]);
    }
};
