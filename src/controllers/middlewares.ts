import jwt from "jsonwebtoken";
import { Context } from "koa";
import { format } from "util";
import { jwtKey } from "../config";
import { User } from "../models/entities/User";
import { getEventById } from "../models/event/get.model";
import { getProfile } from "../models/user/get-profile.model";
import { respondError, respondForbidden, respondInternalError, respondUnauthorized } from "./response";

export interface AccessTokenFormat {
    userId: number;
}

function authorizationToToken(authorization?: string): string | undefined {
    if (!authorization) {
        return undefined;
    }
    const tokens = authorization.split(" ");
    if (tokens.length !== 2 || tokens[0] !== "Bearer") {
        return undefined;
    }
    return tokens[1];
}

export const authMiddleware = async (ctx: Context, next: () => Promise<any>) => {
    const accessToken = authorizationToToken(ctx.request.header.authorization);
    if (!accessToken) {
        return respondUnauthorized(ctx);
    }
    try {
        const accessTokenInfo = jwt.verify(accessToken, jwtKey) as AccessTokenFormat;
        const user = await getProfile(accessTokenInfo.userId);
        if (!user) {
            return respondUnauthorized(ctx);
        }
        ctx.state.user = user;
        await next();
    } catch (error) {
        respondUnauthorized(ctx);
    }
};

export const photographerMiddleware = async (ctx: Context, next: () => Promise<any>) => {
    const user: User = ctx.state.user;
    if (!user || user.type !== "photographer") {
        return respondForbidden(ctx);
    }
    await next();
};

export const userEventMiddleware = async (ctx: Context, next: () => Promise<any>) => {
    const event = await getEventById(ctx.request.body);
    const user: User = ctx.state.user;
    if (!event) {
        return respondError(ctx, ["Event not found."]);
    }
    if (!event.service.photographer || !event.customer || !user) {
        return respondInternalError(ctx);
    }
    if ((user.type === "photographer" && user.id !== event.service.photographer.id) ||
    (user.type === "customer" && user.id !== event.customer.id)) {
        return respondForbidden(ctx);
    }
    ctx.state.event = event;
    await next();
};

export const logger = async (ctx: Context, next: () => Promise<any>) => {
    const startTime = new Date();
    await next();
    const endTime = new Date();
    const message = format("%s - - [%s] \"%s %s\" %d %s %dms",
        ctx.request.ip, startTime.toISOString(), ctx.method, ctx.url, ctx.status, ctx.length || 0,
        (endTime.getTime() - startTime.getTime())
    );
    console.log(message);
};
