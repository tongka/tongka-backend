import { IsDate, IsEmail, IsIn, IsNotEmpty, IsOptional, Length, validate } from "class-validator";
import { Context } from "koa";
import { PaymentMethod } from "../../models/payment/pay";
import { userRegister } from "../../models/user/register.model";
import { respondError, respondSuccess } from "../response";

class UserRegisterForm {
    @IsEmail()
    @Length(1, 100)
    public email: string;

    @Length(8)
    public password: string;

    @Length(1, 50)
    public firstname: string;

    @Length(1, 50)
    public lastname: string;

    @IsIn(["photographer", "customer"])
    public type: string;

    @IsIn(["bank account", "credit card"])
    public paymentType: string;

    @IsNotEmpty()
    @Length(10, 12)
    public bankAccountNumber?: string;

    @IsOptional()
    @Length(16, 16)
    public creditCardNumber?: string;

    @IsOptional()
    @IsDate()
    public cardExpireDate?: Date;

    @IsOptional()
    @Length(3, 4)
    public cvv?: string;

    constructor(body: any) {
        this.email = body.email;
        this.password = body.password;
        this.firstname = body.firstname;
        this.lastname = body.lastname;
        this.type = body.type;
        this.paymentType = body.paymentType;
        this.bankAccountNumber = body.bankAccountNumber;
        this.creditCardNumber = body.creditCardNumber;
        this.cardExpireDate = body.cardExpireDate ? new Date(body.cardExpireDate) : undefined;
        this.cvv = body.cvv;
    }
}

export const userRegisterController = async (ctx: Context) => {
    const form = new UserRegisterForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const registrationError = await userRegister(form.email, form.password, form.firstname,
        form.lastname, form.type as ("photographer" | "customer"), form.paymentType as PaymentMethod,
        form.bankAccountNumber, form.creditCardNumber, form.cardExpireDate, form.cvv);
    if (registrationError) {
        return respondError(ctx, [registrationError]);
    }
    respondSuccess(ctx);
};
