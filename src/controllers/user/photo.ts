import { File } from "formidable";
import { Context } from "koa";
import { changeCoverPhoto, changeProfilePhoto } from "../../models/user/photo.model";
import { respondError, respondSuccess } from "../response";

export const postProfilePhotoController = async (ctx: Context) => {
    if (!ctx.request.files || !ctx.request.files.imageFile) {
        return respondError(ctx, ["File not found"]);
    }
    const imageFile: File = ctx.request.files.imageFile;
    if (!imageFile.type.match(/image\/*/)) {
        return respondError(ctx, ["Not an image file"]);
    }
    const user = await changeProfilePhoto(ctx.state.user.id, imageFile);
    respondSuccess(ctx, user);
};

export const postCoverPhotoController = async (ctx: Context) => {
    if (!ctx.request.files || !ctx.request.files.imageFile) {
        return respondError(ctx, ["File not found"]);
    }
    const imageFile: File = ctx.request.files.imageFile;
    if (!imageFile.type.match(/image\/*/)) {
        return respondError(ctx, ["Not an image file"]);
    }
    const user = await changeCoverPhoto(ctx.state.user.id, imageFile);
    respondSuccess(ctx, user);
};
