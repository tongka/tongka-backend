import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { searchPhotographer } from "../../models/user/search-photographer.model";
import { respondError, respondSuccess } from "../response";

class SearchPhotographerForm {
    @IsNotEmpty()
    public q: string;

    constructor(body: any) {
        this.q = body.q;
    }
}

export const searchPhotographerController = async (ctx: Context) => {
    const form = new SearchPhotographerForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const photographers = await searchPhotographer(form.q);
    respondSuccess(ctx, photographers);
};
