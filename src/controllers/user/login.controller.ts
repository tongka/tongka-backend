import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { userLogin } from "../../models/user/login.model";
import { respondError, respondSuccess } from "../response";

class UserLoginForm {
    @IsNotEmpty()
    public email: string;

    @IsNotEmpty()
    public password: string;

    constructor(body: any) {
        this.email = body.email;
        this.password = body.password;
    }
}

export const userLoginController = async (ctx: Context) => {
    const form = new UserLoginForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const accessToken = await userLogin(form.email, form.password);
    if (!accessToken) {
        return respondError(ctx, ["Incorrect email or password"]);
    }
    respondSuccess(ctx, { accessToken });
};
