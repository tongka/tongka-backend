import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { getUnfinishedEvents } from "../../models/event/get.model";
import { authorizeUser } from "../../models/user/login.model";
import { deleteUser } from "../../models/user/user.model";
import { respondError, respondSuccess } from "../response";

class UserDeleteForm {
    @IsNotEmpty()
    public password: string;

    constructor(body: any) {
        this.password = body.password;
    }
}

export const userDeleteController = async (ctx: Context) => {
    const form = new UserDeleteForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const passwordCorrect = await authorizeUser(ctx.state.user.id, form.password);
    if (!passwordCorrect) {
        return respondError(ctx, ["Incorrect password"]);
    }
    try {
        const openingEvents = await getUnfinishedEvents(ctx.state.user.id);
        if (openingEvents.length > 0) {
            return respondError(ctx, ["Cannot delete user. There are uncompleted events."]);
        }
        await deleteUser(ctx.state.user.id);
        respondSuccess(ctx);
    } catch (e) {
        respondError(ctx, ["User not found"]);
    }
};
