import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { getProfile } from "../../models/user/get-profile.model";
import { respondError, respondSuccess } from "../response";

class GetProfileForm {
    @IsNotEmpty()
    public id: number;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const myProfileController = async (ctx: Context) => {
    const user = await getProfile(ctx.state.user.id);
    respondSuccess(ctx, user);
};

export const userProfileController = async (ctx: Context) => {
    const form = new GetProfileForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const user = await getProfile(form.id);
    respondSuccess(ctx, user);
};
