import { IsDate, IsIn, IsOptional, Length, validate } from "class-validator";
import { Context } from "koa";
import { editProfile } from "../../models/user/edit-profile.model";
import { respondError, respondSuccess } from "../response";

class EditProfileForm {
    @IsOptional()
    @Length(1)
    public firstname?: string;

    @IsOptional()
    @Length(1)
    public lastname?: string;

    @IsOptional()
    @Length(1)
    public bio?: string;

    @IsOptional()
    @IsDate()
    public birthday?: Date;

    @IsOptional()
    @Length(10, 10)
    public phone?: string;

    @IsOptional()
    @Length(1)
    public facebook?: string;

    @IsOptional()
    @Length(1)
    public instagram?: string;

    @IsOptional()
    @Length(1)
    public lineId?: string;

    @IsOptional()
    @IsIn(["bank account", "credit card"])
    public paymentType: "bank account" | "credit card";

    @IsOptional()
    @Length(10, 12)
    public bankAccountNumber?: string;

    @IsOptional()
    @Length(16, 16)
    public creditCardNumber?: string;

    @IsOptional()
    @IsDate()
    public cardExpireDate?: Date;

    @IsOptional()
    @Length(3, 4)
    public cvv?: string;

    constructor(body: any) {
        this.firstname = body.firstname;
        this.lastname = body.lastname;
        this.bio = body.bio;
        this.birthday = body.birthday ? new Date(body.birthday) : undefined;
        this.phone = body.phone;
        this.facebook = body.facebook;
        this.instagram = body.instagram;
        this.lineId = body.lineId;
        this.paymentType = body.paymentType;
        this.bankAccountNumber = body.bankAccountNumber;
        this.creditCardNumber = body.creditCardNumber;
        this.cardExpireDate = body.cardExpireDate ? new Date(body.cardExpireDate) : undefined;
        this.cvv = body.cvv;
    }
}

export const editProfileController = async (ctx: Context) => {
    const form = new EditProfileForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const user = await editProfile(ctx.state.user.id, form.firstname, form.lastname, form.bio, form.birthday,
        form.phone, form.facebook, form.instagram, form.lineId,
        form.paymentType, form.bankAccountNumber, form.creditCardNumber, form.cardExpireDate, form.cvv);
    if (!user) {
        return respondError(ctx, ["Payment information is missing."]);
    }
    respondSuccess(ctx, user);
};
