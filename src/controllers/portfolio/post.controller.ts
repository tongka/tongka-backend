import { File } from "formidable";
import { Context } from "koa";
import sharp from "sharp";
import { uploadPhoto } from "../../models/photo/upload.model";
import { respondError, respondSuccess } from "../response";

export const postPortfolioController = async (ctx: Context) => {
    if (!ctx.request.files || !ctx.request.files.imageFile) {
        return respondError(ctx, ["File not found"]);
    }
    const imageFile: File = ctx.request.files.imageFile;
    if (!imageFile.type.match(/image\/*/)) {
        return respondError(ctx, ["Not an image file"]);
    }
    const photo = await uploadPhoto(imageFile, ctx.state.user, sharp(), 1);
    respondSuccess(ctx, photo);
};
