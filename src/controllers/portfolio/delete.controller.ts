import { IsNotEmpty, validate } from "class-validator";
import { Context } from "koa";
import { getPhotoById, removePortfolio } from "../../models/photo/photo.model";
import { respondError, respondForbidden, respondSuccess } from "../response";

class DeletePortfolioForm {
    @IsNotEmpty()
    public photoId: string;

    constructor(body: any) {
        this.photoId = body.photoId;
    }
}

export const deletePortfolioController = async (ctx: Context) => {
    const form = new DeletePortfolioForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const photo = await getPhotoById(form.photoId);
    if (!photo || photo.owner!.id !== ctx.state.user.id) {
        return respondForbidden(ctx);
    }
    await removePortfolio(form.photoId);
    respondSuccess(ctx);
};
