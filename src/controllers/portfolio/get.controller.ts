import { IsNotEmpty, IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { getUserPortfolio } from "../../models/photo/photo.model";
import { respondError, respondSuccess } from "../response";

class GetPortfolioForm {
    @IsNotEmpty()
    @IsNumberString()
    public userId: string;

    constructor(body: any) {
        this.userId = body.userId;
    }
}

export const getPortfolioController = async (ctx: Context) => {
    const form = new GetPortfolioForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const photos = await getUserPortfolio(parseInt(form.userId, 10));
    respondSuccess(ctx, photos);
};
