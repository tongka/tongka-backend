import dotenv from "dotenv";
import { join } from "path";
import { ConnectionOptions } from "typeorm";

dotenv.config();

export const serverPort = parseInt(process.env.PORT || "3000", 10);
export const jwtKey = process.env.JWT_KEY || "tongkatakong_sesec2";
export const dbConfig: ConnectionOptions = {
    type: "mysql",
    host: process.env.MYSQL_HOST || "localhost",
    username: process.env.MYSQL_USER || "root",
    password: process.env.MYSQL_PASSWORD || "",
    database: process.env.MYSQL_DATABASE || "tongka",
    port: parseInt(process.env.MYSQL_PORT || "3306", 10),
    synchronize: true,
    migrationsTableName: "migrations",
    migrationsRun: true,
    entities: [
        join(__dirname + "/models/entities/*.[t|j]s")
    ],
    migrations: [
        join(__dirname + "/models/migrations/*.[t|j]s")
    ]
};
export const bankBaseUrl = process.env.BANK_URL || "http://localhost:3500";
